package nl.hva.dmci.ict.se.datastructures;

import java.util.ArrayList;
import java.util.List;

public class Yolo implements DictionaryGenerator {


    public String[] yololian(int n) {
        //base strings. Deze vormen de basis voor elke bijkomende variatie van yo en lo
        String yo = "yo";
        String lo = "lo";
        //Base case. Als n gelijk staat aan 1 heeft hij het aantal benodigde iteraties uitgevoerd om alle lettergrepen toe te voegen
        if (n == 1) {
            return new String[]{yo, lo};
        }

        List<String> list = new ArrayList<String>();
        //als N niet 1 is, voer de functie opnieuw uit en voeg voeg bij elke bestaande string yo en lo toe.
        //Doe hierna N-1, oftewel er is een lettergreep toegevoegd.
        for (String str: yololian(n - 1)) {
            //add de huidige string + yo
            list.add(str + yo);
            //add de huidige string + lo
            list.add(str + lo);
            System.out.println("current array: " + list);
        }
        //convert de arraylist naar een string array en return  returnt daarmee voor beide begin cases alle possible arrays (yo en lo)
        return list.toArray(new String[n]);
}


    public String[] language(int n, String[] syllables) {
        return new String[0];
    }
}
