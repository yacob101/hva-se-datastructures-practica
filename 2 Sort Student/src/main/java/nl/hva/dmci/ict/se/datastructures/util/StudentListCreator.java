package nl.hva.dmci.ict.se.datastructures.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <Describe the purpose of this class>
 *
 * @author <your name goes here>
 */
public class StudentListCreator {

    public Object returnObjects;



    public returnObjects createStudentList( int aantalStudenten) {
        //Het return object is de arraylist van Cijferoccurences en de List studielijst. Dit is een apart object zodat
        //deze met een methode gegenereerd kunnen worden
        returnObjects returnObject = new returnObjects();
        int amountOfStudents = aantalStudenten;
        //generate een array van Student objects
        Student[] studentenLijst = Student.generateStudents(amountOfStudents);
        System.out.println("Hier moet het gaan gebeuren....");
        //Schud de lijst met students


        //Makes an empty arraylist of CijferOccurences objects
        ArrayList<CijferOccurences> possiblecijfers = new ArrayList<CijferOccurences>();

        //Counter voor het aantal keren dat een nummer voorkomt
        for (int i = 0; i < studentenLijst.length; i++) {
            //Stud is het huidige student object
            Student stud = studentenLijst[i];
            double studCijfer = stud.getCijfer();
            boolean b = false;

//            J is voor possiblecijfers
            for (CijferOccurences cijfer : possiblecijfers) {
                if (cijfer.getCijfer() == studCijfer) {
                    System.out.println("Found existing number " + studCijfer);
                    cijfer.setOccurences(cijfer.getOccurences() + 1);
                    System.out.println(studCijfer + "OCCURS " + cijfer.getOccurences() + " TIMES.");
                    b = true;
                    break;
                }
            }
            if (b == false) {
                //Add nieuw cijfer object met het huidige student cijfer.
                possiblecijfers.add(new CijferOccurences(studCijfer));
                System.out.println(studCijfer + " New cijfer Has been added");
            }
        }
        Collections.sort(possiblecijfers);

        returnObject.possiblecijfers = possiblecijfers;
        returnObject.studielijst = selectionSortstudents(studentenLijst); //sort de studenten lijst
        //returnObject.studielijst = new LinkedList<>(Arrays.asList(studentenLijst));
        return returnObject;
        }

public ArrayList<Student> selectionSortstudents(Student[] studentArray) {

        ArrayList<Student> studentArrayList = new ArrayList<Student>(Arrays.asList(studentArray));
        for (int i = 0; i < studentArrayList.size(); i++) {
        // find position of smallest num between (i + 1)th element and last element
        int pos = i;
        for (int j = i; j < studentArrayList.size(); j++) {
        if (studentArrayList.get(j).compareTo(studentArrayList.get(pos)) < 0)
        pos = j;
        }
        // Swap min (smallest num) to current position on array
        Student min = studentArrayList.get(pos);
        studentArrayList.set(pos, studentArrayList.get(i));
        studentArrayList.set(i, min);
        }
        return studentArrayList;
        }



class returnObjects {
    ArrayList<CijferOccurences> possiblecijfers = new ArrayList<CijferOccurences>();
    List<Student> studielijst;
}
}


