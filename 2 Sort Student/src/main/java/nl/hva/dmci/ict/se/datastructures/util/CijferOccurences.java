package nl.hva.dmci.ict.se.datastructures.util;

public class CijferOccurences implements Comparable<CijferOccurences>{

    private double cijfer;
    private int occurences = 1;

    public CijferOccurences(double cijf){
        cijfer = cijf;
    }
    public double getCijfer() {
        return cijfer;
    }

    public void setCijfer(double cijfer) {
        this.cijfer = cijfer;
    }

    public int getOccurences() {
        return occurences;
    }

    public void setOccurences(int occurences) {
        this.occurences = occurences;
    }

    @Override
    public int compareTo(CijferOccurences otherCijfer) {
        return Double.compare(this.cijfer, otherCijfer.cijfer);
    }

}
