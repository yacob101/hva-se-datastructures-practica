package nl.hva.dmci.ict.se.datastructures.util;

/**
 * Containing the advanced sort algorithms.
 * Currently only selectionsort is implemented.
 * 
 * @author Nico Tromp
 */
public class AdvancedSorts {
    
    private static void swap(Student[] students, int smallest, int selection) {
        Student temp = students[selection];
        students[selection] = students[smallest];
        students[smallest] = temp;
    }
}
