package nl.hva.dmci.ict.se.datastructures.util;

import jxl.Workbook;
import jxl.write.*;
import jxl.write.Number;
import nl.hva.dmci.ict.se.datastructures.Deque;
import nl.hva.dmci.ict.se.datastructures.Node;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <Describe the purpose of this class>
 *
 * @author <your name goes here>
 */
public class Main {
    private static final String EXCEL_FILE_LOCATION = "C:\\temp\\2B StudentCijfers.xls";

    public static void main(String[] args) {
        RijCont rijtjesControle = new RijCont();
        StudentListCreator listCreator = new StudentListCreator();
        ExcelWrite excelWriter = new ExcelWrite();
        int aantalStudenten = 5;


        //maakt een object waarin returnobjects ge returned kan worden
        listCreator.returnObjects = listCreator.createStudentList(aantalStudenten);
        //Haalt de arraylist met possiblecijfers uit returnObjects
        ArrayList<CijferOccurences> studentCijfers = ((StudentListCreator.returnObjects) listCreator.returnObjects).possiblecijfers;
        System.out.println("time to print the entire list");
        //write studentlist in excel
        List<Student> studielijst = ((StudentListCreator.returnObjects) listCreator.returnObjects).studielijst;


        Deque<Student> studentDeque = new Deque<Student>();
        studentDeque.pushRight(studielijst.get(0));
        studentDeque.pushRight(studielijst.get(1));
        studentDeque.pushRight(studielijst.get(2));
        studentDeque.pushRight(studielijst.get(3));
        studentDeque.pushRight(studielijst.get(4));

        System.out.println(studielijst);

        System.out.println("print na adden");
        for (Student stud : studentDeque) {
            System.out.println(stud);
        }
//        for (Student stud : studentDeque) {
//            System.out.println(stud);
//        }System.out.println("print 1");


//        for (Student stud : studentDeque) {
//            System.out.println(stud);
//        }System.out.println("print 2");
//        System.out.println(studielijst);
//        System.out.println(studielijst);

        Node<Student> dequeHead;

        System.out.println("print voor adden");
        dequeHead= studentDeque.getHead();
        while (dequeHead.getNext() != null){
            System.out.println(dequeHead.getItem());
            dequeHead = dequeHead.getNext();
        }

        studentDeque = studentDeque.addSorted(studielijst.get(2), studentDeque);
        System.out.println("print na adden");
         dequeHead= studentDeque.getHead();
        while (dequeHead.getNext() != null){
            System.out.println(dequeHead.getItem());
            dequeHead = dequeHead.getNext();
        }
        System.out.println("print na adden");
        for (Student stud : studentDeque) {
            System.out.println(stud);
        }


//        Collections.sort(studielijst);
   //     rijtjesControle.isStijgend(studielijst);

    }
}
