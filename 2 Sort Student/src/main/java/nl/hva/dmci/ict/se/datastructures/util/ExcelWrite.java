package nl.hva.dmci.ict.se.datastructures.util;

import jxl.Workbook;
import jxl.write.*;
import jxl.write.Number;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class ExcelWrite {

    private static final String EXCEL_FILE_LOCATION = "C:\\temp\\2B StudentCijfers.xls";

    WritableWorkbook myFirstWbook = null;
    public void writeExcel(ArrayList<CijferOccurences> studentCijfers){
        try {

            myFirstWbook = Workbook.createWorkbook(new File(EXCEL_FILE_LOCATION));
            // create an Excel sheet
            WritableSheet excelSheet = myFirstWbook.createSheet("Sheet 1", 0);
            // add something into the Excel sheet
            Label label = new Label(0, 0, "Cijfer");
            excelSheet.addCell(label);
            label = new Label(1, 0, "Aantal");
            excelSheet.addCell(label);
            int i = 0;
            for (CijferOccurences cijferObject : studentCijfers) {
                System.out.println(cijferObject.getCijfer() + " occurs : " + cijferObject.getOccurences() + " times");
                // Add current cijfer and occurance to excel sheet
                Number number = new Number(0, i, cijferObject.getCijfer());
                excelSheet.addCell(number);
                number = new Number(1, i, cijferObject.getOccurences());
                excelSheet.addCell(number);
                i++;
            }
            myFirstWbook.write();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        } finally {
            if (myFirstWbook != null) {
                try {
                    myFirstWbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (WriteException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    }