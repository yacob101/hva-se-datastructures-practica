//Deze implement de interface RijtjesControle om de methode Stijgende Rijen te gebruiken

package nl.hva.dmci.ict.se.datastructures.util;

import java.util.List;

public class RijCont implements RijtjesControle {
    int n = 0;

    @Override
    public <T extends Comparable<T>> boolean isDalend(List<T> rijtje) {
        //als rijtje.size 1 is, end de comparison
        System.out.println(" aantal elements left:" + rijtje.size());
        if (rijtje.size() == 1) {
            System.out.println("Stijgende rij confirmed");
            return true;
        }

        //compares first slot in rijtje to second slot. -1 = bigger, +1 = smaller.
        int i = rijtje.get(0).compareTo(rijtje.get(1));

        //second slot is bigger than first slot
        if (i == -1) {
            System.out.println((rijtje.get(1)) + " is bigger than " + (rijtje.get(0)));
            rijtje.remove(0);
            isDalend(rijtje);
        }
        //2nd slot is smaller than first slot, rijtje is niet stijgend, return false.
        if (i == 1) {
            System.out.println((rijtje.get(1)) + " is smaller than " + (rijtje.get(0)));
            System.out.println("Rijtje is niet stijgend");
            return false;
            //        int a = rijtje.get(n).compareTo(rijtje.get(n+1));
            //        return false;
        }
        return false;
    }

    @Override
    public <T extends Comparable<T>> boolean isStijgend(List<Student> rijtje) {
        //als rijtje.size 1 is, end de comparison
        System.out.println(" aantal elements left:" + rijtje.size());
        if (rijtje.size() == 1) {
            System.out.println("Stijgende rij confirmed");
            return true;
        }

        //compares first slot in rijtje to second slot. -1 = bigger, +1 = smaller.
        int i = rijtje.get(0).compareTo(rijtje.get(1));

        //second slot is bigger than first slot
        if (i <0) {
            System.out.println((rijtje.get(1)) + " is bigger than " + (rijtje.get(0)));
            rijtje.remove(0);
            isStijgend(rijtje);
        }
        //2nd slot is smaller than first slot, rijtje is niet stijgend, return false.
        if (i == 1) {
            System.out.println((rijtje.get(1)) + " is smaller than " + (rijtje.get(0)));
            System.out.println("Rijtje is niet stijgend");
            return false;
            //        int a = rijtje.get(n).compareTo(rijtje.get(n+1));
            //        return false;
        }

        return false;
    }


    @Override
    public <T extends Comparable<T>> boolean isGelijk(List<T> rijtje) {
        return false;
    }
}
