package nl.hva.dmci.ict.se.datastructures.util;

import java.security.SecureRandom;
import java.util.Random;

/**
 * A <code>Student</code> holds the information for a student which takes the Datastructures course
 * at the HvA.</br>
 * Students have to create this class.
 *
 * @author Nico Tromp
 */
public class Student implements Comparable<Student>{
    private static int volgendeStudentID = 500800001;
    private static Random randomizer = new SecureRandom();
    private int studentID;
    private double cijfer;

    public Student() {
        studentID = volgendeStudentID++;
        cijfer = (10 + randomizer.nextInt(91)) / 10.0;
    }

    public static Student[] generateStudents(int numberOfStudents) {
        Student[] students = new Student[numberOfStudents];
        for (int i = 0; i < numberOfStudents; i++) {
            students[i] = new Student();
        }
        return students;
    }

    public double getCijfer() {
        return cijfer;
    }

    public int getStudentID() {
        return studentID;
    }

    @Override
    public String toString() {
        return String.format("%9d, %.1f", studentID, cijfer);
    }

    @Override
    public int compareTo(Student o) {
        int gradeCompare =  ((int) (10.0 * cijfer)) - ((int)  (10.0 * o.cijfer)) ;
        if (gradeCompare == 0) {
            return ( studentID - o.studentID);
        } else {
            return gradeCompare;
        }
    }
}




