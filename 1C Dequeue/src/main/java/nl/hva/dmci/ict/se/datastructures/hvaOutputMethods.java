package nl.hva.dmci.ict.se.datastructures;

import java.util.Iterator;

public class hvaOutputMethods {


    public void wordDeque() {
        Deque<String> words = new Deque<String>();
        words.pushLeft("Datastructuren");
        words.pushLeft("is");
        words.pushRight("heel");
        words.pushRight("leuk");
        words.pushLeft("of");
        words.pushRight("niet?");
        words.changeLeft(3, "test");
        words.changeRight(3, "een");
        words.changeLeft(5, "Dit");
        words.changeLeft(0, words.popRight());
        words.popRight();

        for (String word : words) {
            System.out.println(word);
        }
    }

    public void intDeque() {

        Deque<Integer> numbers = new Deque<>();
        numbers.pushLeft(1);
        numbers.pushRight(2);
        numbers.pushLeft(3);
        numbers.pushLeft(numbers.popLeft() - numbers.popRight());
        numbers.pushRight(2);
        numbers.pushLeft(5);
        numbers.pushLeft(5);
        numbers.pushRight(8);
        numbers.pushRight(numbers.popRight() - numbers.popLeft());
        numbers.pushRight(3);
        numbers.pushRight(9);
        numbers.pushRight(13);
        numbers.pushRight(7 * numbers.changeLeft(4, numbers.popLeft()));
        numbers.changeRight(2, 8);
        numbers.changeLeft(6, numbers.changeRight(1, 0));

        for (int number : numbers) {
            System.out.println(number);

        }
//        Iterator<Integer> ints = numbers.iterator();
//        for (Iterator<Integer> it = ints; it.hasNext(); ) {
//            Integer i = it.next();
//            System.out.println(i);
//        }
    }
}


