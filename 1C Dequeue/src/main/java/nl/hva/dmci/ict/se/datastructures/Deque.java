package nl.hva.dmci.ict.se.datastructures;

import nl.hva.dmci.ict.se.datastructures.util.Student;

import java.util.Iterator;

public class Deque<T> implements DoubleEndedQueue<T>, Iterable<T> {
    private Node head;

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }

    private Node tail;
    private int size = 0;

    public Deque() {
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public int size() {
        return this.size;
    }



    //voegt een item toe aan de linkerkant van de double ended queue

    public Deque<Student> addSorted(Student item, Deque<Student> deque) {
        Node toAdd = new Node(item, null, (Node) null);
        if (this.head != null) {
            this.head.previous = toAdd;
        }
        System.out.println();
        System.out.println( " Het item is " + item);
        System.out.println();
        System.out.println((Student) this.head.item);
        System.out.println(item.compareTo((Student) this.head.item));
        while ((item.compareTo((Student) this.head.item)) > 0) {
            this.head = this.head.next;
        }
        {
        System.out.println("comparison met huidige node " + (item.compareTo((Student) this.head.item)));

        //tail van deze node is de tail van vorige
        toAdd.previous = this.head;
        toAdd.next = this.head.next;
        toAdd.previous.next = toAdd;
        System.out.println("added ding");
        toAdd.next.previous = toAdd;
//            System.out.println("itempje = " +toAdd.item);
//            System.out.println("itempje.prev = " +toAdd.previous.item);
//            System.out.println("itempje.next = " +toAdd.next.item);
//            System.out.println("itempje.next prev = " +toAdd.next.previous.item);
        ++this.size;
    }
        return deque;

    }

    public void addTest(Student item){
        Node<Student> toAdd = new Node(item, null, (Node)null);
        Node<Student> current = head;
        Node<Student> previous = null;
        while (current != null && ((current.item.compareTo(item)) <0)) {
            previous = current;
            System.out.println("current item = "+ current.item );
            current = current.next;
        }

        Node<Student> node = new  Node<Student>(item,null,null);

        if (previous == null) {
            head = node;
        } else {
            // re-link previous node
            previous.next = node;
            node.previous = previous;
        }

        if (current != null) {
            // re-link next node
            current.previous = node;
            node.next = current;
        }
        ++this.size;
    }


    public void pushLeft(T item) {
        Node toAdd = new Node(item, this.head, (Node)null);
        if (this.head != null) {
            this.head.previous = toAdd;
        }

        this.head = toAdd;
        if (this.tail == null) {
            this.tail = toAdd;
        }

        ++this.size;
    }
    //voegt een item toe aan de rechterkant van de double ended queue
    public void pushRight(T item) {
        Node toAdd = new Node(item, (Node)null, this.tail);
        if (this.tail != null) {
            this.tail.next = toAdd;
        }

        this.tail = toAdd;
        if (this.head == null) {
            this.head = toAdd;
        }

        ++this.size;
    }
    //haalt de head(linkerkant) van een node weg en koppelt de head van die node aan de huidige node
    public T popLeft() {
        if (this.size != 0) {
            //maakt een backup van de
            Node<T> toRemove = this.head;
            this.head = this.head.next;
            this.head.previous = null;
            --this.size;
            return toRemove.item;
        } else {
            return null;
        }
    }
    //haalt de tail(rechterkant) van een node weg en koppelt de tail van die node aan de huidige node
    public T popRight() {
        if (this.size != 0) {
            Node<T> toRemove = this.tail;
            this.tail = this.tail.previous;
            this.tail.next = null;
            --this.size;
            return toRemove.item;
        } else {
            return null;
        }
    }
    //Vervangt het element op de <code>n</code>de plaats van rechts  met de waarde van
        //item en geeft de oorspronkelijke waarde van het element terug.
    public T changeLeft(int n, T newItem) {
        //kopiert de head node
        //node = een nieuwe node met de gegevens van de tailnode
        Node<T> node = this.tail;

        T old;
        //zolang er verder ge iterate kan worden en er een head node aanwezig is, naar de tail van de node totdat N bereikt is
        //kopiert het item van de huidige node
        for(old = node.item; node != null && n > 0; --n) {
            node = node.previous;
        }
        //als de node waarnaar ge iterate is bestaat, vervang het item met het item uit de parameter
        if (node != null) {
            node.item = newItem;
        } else {
            //als de node niet bestaat print "cant L"
            System.out.println("cant L");
        }
        //return old (oude node.item)
        return old;
    }

    public T changeRight(int n, T newItem) {
        Node<T> node = this.head;

         T old;
        for(old = node.item; node != null && n > 0; --n) {
            node = node.next;
        }

        if (node != null) {
            node.item = newItem;
        } else {
            System.out.println("cant R");
        }

        return old;
    }

    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node temp;

            {
                this.temp = Deque.this.head;
            }

            public boolean hasNext() {
                return this.temp != null;
            }

            public T next() {
                T toReturn = (T) this.temp.item;
                this.temp = this.temp.next;

                return toReturn;
            }
        };
    }
}