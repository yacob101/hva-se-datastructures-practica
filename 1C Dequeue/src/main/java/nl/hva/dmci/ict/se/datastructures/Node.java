package nl.hva.dmci.ict.se.datastructures;

public class Node<T> {
    T item;
    Node next;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getPrevious() {
        return previous;
    }

    public void setPrevious(Node previous) {
        this.previous = previous;
    }

    Node previous;
// makes a node with an item, a next (tail) and a previous (head)
    public Node(T item, Node next, Node previous) {
        this.item = item;
        this.next = next;
        this.previous = previous;
    }
}
